function [p11,p12,p13,p14,p15,p16,p17,p21,p22,p23,p24,p25,p26,p27,p31,p32,p33,p34,p35,p36,p37,p41,p42,p43,p44,p45,p46,p47] = func_fwd_expand(x,p17expand,p27expand,p37expand,p47expand)


params;

% rotation around x
Z = @(z,q)   [1       0          0       0;...
              0   cos(q)    -sin(q)      0;...
              0   sin(q)     cos(q)      z;...
              0       0          0       1] ;

% rotation around y
Y = @(y,q)       [   cos(q)     0     sin(q)  0 ;...
                         0      1         0   y;...
                    -sin(q)     0     cos(q)  0;...
                         0      0         0   1];
% translation matrix

T = @(x,y,z)   [1   0   0   x;...
                0   1   0   y;...
                0   0   1   z;...
                0   0   0   1];
            

            
%%            
%from origin to hip1
T017 = T(p17expand(1),p17expand(2),p17expand(3));
% tansformation matrix from hip to feet in leg1
% point 7 has the same orientation with world coordinate
T177_pr = Y(l5,x(3));
T17_pr6 = Z(0,pi/2-x(2));
T165 = Z(-l4,0);
T153 = Z(-l2,-(pi-x(1)));
T132 = Z(-l3,pi-x(1));
T121 = Z(-l1,-(pi/2-x(2)));
T110 = Y(0,-x(3));

T165_pr = Z(-l4,x(1));
T154 = Z(l3,-x(1)); 

            
%calculate each joint position
P17 = T017;
p17 = P17(1:3,4);
P16 = T017*T177_pr*T17_pr6;
p16 = P16(1:3,4);
P15 = T017*T177_pr*T17_pr6*T165;
p15 = P15(1:3,4);
P14 = T017*T177_pr*T17_pr6*T165_pr*T154;
p14 = P14(1:3,4);
P13 = T017*T177_pr*T17_pr6*T165*T153;
p13 = P13(1:3,4);
P12 = T017*T177_pr*T17_pr6*T165*T153*T132;
p12 = P12(1:3,4);
P11 = T017*T177_pr*T17_pr6*T165*T153*T132*T121;
p11 = P11(1:3,4);

%%
%from origin to hip2
T027 = T(p27expand(1),p27expand(2),p27expand(3));
% tansformation matrix from hip to feet in leg2
T277_pr = Y(l5,x(6));
T27_pr6 = Z(0,pi/2-x(5));
T265 = Z(-l4,0);
T253 = Z(-l2,-(pi-x(4)));
T232 = Z(-l3,pi-x(4));
T221 = Z(-l1,-(pi/2-x(5)));
T210 = Y(0,-x(6));

T265_pr = Z(-l4,x(4));
T254 = Z(l3,-x(4)); 

%calculate each joint position
P27 = T027;
p27 = P27(1:3,4);
P26 = T027*T277_pr*T27_pr6;
p26 = P26(1:3,4);
P25 = T027*T277_pr*T27_pr6*T265;
p25 = P25(1:3,4);
P24 = T027*T277_pr*T27_pr6*T265_pr*T254;
p24 = P24(1:3,4);
P23 = T027*T277_pr*T27_pr6*T265*T253;
p23 = P23(1:3,4);
P22 = T027*T277_pr*T27_pr6*T265*T253*T232;
p22 = P22(1:3,4);
P21 = T027*T277_pr*T27_pr6*T265*T253*T232*T221;
p21 = P21(1:3,4);

%%
%from origin to hip3
T037 = T(p37expand(1),p37expand(2),p37expand(3));
% tansformation matrix from hip to feet in leg3
T377_pr = Y(l5,x(9));
T37_pr6 = Z(0,pi/2-x(8));
T365 = Z(-l4,0);
T353 = Z(-l2,-(pi-x(7)));
T332 = Z(-l3,pi-x(7));
T321 = Z(-l1,-(pi/2-x(8)));
T310 = Y(0,-x(9));

T365_pr = Z(-l4,x(7));
T354 = Z(l3,-x(7)); 

%calculate each joint position
P37 = T037;
p37 = P37(1:3,4);
P36 = T037*T377_pr*T37_pr6;
p36 = P36(1:3,4);
P35 = T037*T377_pr*T37_pr6*T365;
p35 = P35(1:3,4);
P34 = T037*T377_pr*T37_pr6*T365_pr*T354;
p34 = P34(1:3,4);
P33 = T037*T377_pr*T37_pr6*T365*T353;
p33 = P33(1:3,4);
P32 = T037*T377_pr*T37_pr6*T365*T353*T332;
p32 = P32(1:3,4);
P31 = T037*T377_pr*T37_pr6*T365*T353*T332*T321;
p31 = P31(1:3,4);

%%
%from origin to hip4
T047 = T(p47expand(1),p47expand(2),p47expand(3));
% tansformation matrix from hip to feet in leg4
T477_pr = Y(l5,x(12));
T47_pr6 = Z(0,pi/2-x(11));
T465 = Z(-l4,0);
T453 = Z(-l2,-(pi-x(10)));
T432 = Z(-l3,pi-x(10));
T421 = Z(-l1,-(pi/2-x(11)));
T410 = Y(0,-x(12));

T465_pr = Z(-l4,x(10));
T454 = Z(l3,-x(10)); 

%calculate each joint position
P47 = T047;
p47 = P47(1:3,4);
P46 = T047*T477_pr*T47_pr6;
p46 = P46(1:3,4);
P45 = T047*T477_pr*T47_pr6*T465;
p45 = P45(1:3,4);
P44 = T047*T477_pr*T47_pr6*T465_pr*T454;
p44 = P44(1:3,4);
P43 = T047*T477_pr*T47_pr6*T465*T453;
p43 = P43(1:3,4);
P42 = T047*T477_pr*T47_pr6*T465*T453*T432;
p42 = P42(1:3,4);
P41 = T047*T477_pr*T47_pr6*T465*T453*T432*T421;
p41 = P41(1:3,4);



            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
end