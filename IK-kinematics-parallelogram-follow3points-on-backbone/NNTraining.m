% This file is to use nerual network to perform inverse kinematics of
% quadruped Husky.
% First, generate the training data, given the trajectories(randomly),
% using fmincon to generate the joints angle, and then use the data to
% train neural network.



clear all;
close all;
clc;

% obtain the params from the CAD model
params;

% set the training data number 
 N = 2e2;
 
%  initiate the training angles of the first leg
q1 = 1+1*rand(1,N);
q2 = 0.7+0.4*rand(1,N);
q3 = -0.05-0.04*rand(1,N);

% set initial guess for fsolve to solve for other leg angles based on the physical constraints 
x0 = [1.45 0.85 0.06 1.45 0.85 -0.06 1.45 0.85 0.06];


q = [q1;q2;q3];
options = optimset('Display','off');

% calculated all the joint angles based on the constraints
for i = 1:N

% myfun = @constrainedQ;
F = @(x) constrainedQ(x,q(:,i));
output = fsolve(F,x0,options);

q4(1,i) = output(1);
q5(1,i) = output(2);
q6(1,i) = output(3);
q7(1,i) = output(4);
q8(1,i) = output(5);
q9(1,i) = output(6);
q10(1,i) = output(7);
q11(1,i) = output(8);
q12(1,i)= output(9);
end

% wrap all the data to generate the training data
x_training = [q1;q2;q3;q4;q5;q6;q7;q8;q9;q10;q11;q12];


% generate position of the robot based on the given training angles
for i = 1:N
[~,~,~,~,~,~,...
    p11_training(:,i),p12_training(:,i),p13_training(:,i),p14_training(:,i),p15_training(:,i),p16_training(:,i),p17_training(:,i),...
    p21_training(:,i),p22_training(:,i),p23_training(:,i),p24_training(:,i),p25_training(:,i),p26_training(:,i),p27_training(:,i),...
    p31_training(:,i),p32_training(:,i),p33_training(:,i),p34_training(:,i),p35_training(:,i),p36_training(:,i),p37_training(:,i),...
    p41_training(:,i),p42_training(:,i),p43_training(:,i),p44_training(:,i),p45_training(:,i),p46_training(:,i),p47_training(:,i),...
    pb1_training(:,i),~,~] = func_fwd_kinematics(x_training(:,i));
end

%%

% plot the training data of robot sitting down 
% converted the data to feed into neural network
p11_training = p11_training';
p12_training = p12_training';
p13_training = p13_training';
p14_training = p14_training';
p15_training = p15_training';
p16_training = p16_training';
p17_training = p17_training';

p21_training = p21_training';
p22_training = p22_training';
p23_training = p23_training';
p24_training = p24_training';
p25_training = p25_training';
p26_training = p26_training';
p27_training = p27_training';

p31_training = p31_training';
p32_training = p32_training';
p33_training = p33_training';
p34_training = p34_training';
p35_training = p35_training';
p36_training = p36_training';
p37_training = p37_training';

p41_training = p41_training';
p42_training = p42_training';
p43_training = p43_training';
p44_training = p44_training';
p45_training = p45_training';
p46_training = p46_training';
p47_training = p47_training';


% set markersize of the scatter3
s = 3;
% plot the training data
figure;
joint1 = scatter3(p11_training(:,1),p11_training(:,2),p11_training(:,3),s,'MarkerFaceColor','k');
hold on
joint2 = scatter3(p12_training(:,1),p12_training(:,2),p12_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
joint3 = scatter3(p13_training(:,1),p13_training(:,2),p13_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
joint4 = scatter3(p14_training(:,1),p14_training(:,2),p14_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
joint5 = scatter3(p15_training(:,1),p15_training(:,2),p15_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
joint6 = scatter3(p16_training(:,1),p16_training(:,2),p16_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
joint7 = scatter3(p17_training(:,1),p17_training(:,2),p17_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p21_training(:,1),p21_training(:,2),p21_training(:,3),s,'MarkerFaceColor','k');
scatter3(p22_training(:,1),p22_training(:,2),p22_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p23_training(:,1),p23_training(:,2),p23_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p24_training(:,1),p24_training(:,2),p24_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p25_training(:,1),p25_training(:,2),p25_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p26_training(:,1),p26_training(:,2),p26_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p27_training(:,1),p27_training(:,2),p27_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p31_training(:,1),p31_training(:,2),p31_training(:,3),s,'MarkerFaceColor','k');
scatter3(p32_training(:,1),p32_training(:,2),p32_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p33_training(:,1),p33_training(:,2),p33_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p34_training(:,1),p34_training(:,2),p34_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p35_training(:,1),p35_training(:,2),p35_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p36_training(:,1),p36_training(:,2),p36_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p37_training(:,1),p37_training(:,2),p37_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p41_training(:,1),p41_training(:,2),p41_training(:,3),s,'MarkerFaceColor','k');
scatter3(p42_training(:,1),p42_training(:,2),p42_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p43_training(:,1),p43_training(:,2),p43_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p44_training(:,1),p44_training(:,2),p44_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p45_training(:,1),p45_training(:,2),p45_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p46_training(:,1),p46_training(:,2),p46_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p47_training(:,1),p47_training(:,2),p47_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');
% plot first leg
    plot3([p11_training(N,1),p12_training(N,1)],[p11_training(N,2),p12_training(N,2)],[p11_training(N,3),p12_training(N,3)],'color','k');
    plot3([p12_training(N,1),p14_training(N,1)],[p12_training(N,2),p14_training(N,2)],[p12_training(N,3),p14_training(N,3)],'color','k');
    plot3([p12_training(N,1),p13_training(N,1)],[p12_training(N,2),p13_training(N,2)],[p12_training(N,3),p13_training(N,3)],'color','k');
    plot3([p13_training(N,1),p15_training(N,1)],[p13_training(N,2),p15_training(N,2)],[p13_training(N,3),p15_training(N,3)],'color','k');
    plot3([p14_training(N,1),p15_training(N,1)],[p14_training(N,2),p15_training(N,2)],[p14_training(N,3),p15_training(N,3)],'color','k');
    plot3([p15_training(N,1),p16_training(N,1)],[p15_training(N,2),p16_training(N,2)],[p15_training(N,3),p16_training(N,3)],'color','k');
    plot3([p16_training(N,1),p17_training(N,1)],[p16_training(N,2),p17_training(N,2)],[p16_training(N,3),p17_training(N,3)],'color','k');
    
    plot3(p11_training(N,1),p11_training(N,2),p11_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p12_training(N,1),p12_training(N,2),p12_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p13_training(N,1),p13_training(N,2),p13_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p14_training(N,1),p14_training(N,2),p14_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p15_training(N,1),p15_training(N,2),p15_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p16_training(N,1),p16_training(N,2),p16_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p17_training(N,1),p17_training(N,2),p17_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);

% plot second leg
    plot3([p21_training(N,1),p22_training(N,1)],[p21_training(N,2),p22_training(N,2)],[p21_training(N,3),p22_training(N,3)],'color','k');
    plot3([p22_training(N,1),p24_training(N,1)],[p22_training(N,2),p24_training(N,2)],[p22_training(N,3),p24_training(N,3)],'color','k');
    plot3([p22_training(N,1),p23_training(N,1)],[p22_training(N,2),p23_training(N,2)],[p22_training(N,3),p23_training(N,3)],'color','k');
    plot3([p23_training(N,1),p25_training(N,1)],[p23_training(N,2),p25_training(N,2)],[p23_training(N,3),p25_training(N,3)],'color','k');
    plot3([p24_training(N,1),p25_training(N,1)],[p24_training(N,2),p25_training(N,2)],[p24_training(N,3),p25_training(N,3)],'color','k');
    plot3([p25_training(N,1),p26_training(N,1)],[p25_training(N,2),p26_training(N,2)],[p25_training(N,3),p26_training(N,3)],'color','k');
    plot3([p26_training(N,1),p27_training(N,1)],[p26_training(N,2),p27_training(N,2)],[p26_training(N,3),p27_training(N,3)],'color','k');
    
    plot3(p21_training(N,1),p21_training(N,2),p21_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p22_training(N,1),p22_training(N,2),p22_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p23_training(N,1),p23_training(N,2),p23_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p24_training(N,1),p24_training(N,2),p24_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p25_training(N,1),p25_training(N,2),p25_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p26_training(N,1),p26_training(N,2),p26_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p27_training(N,1),p27_training(N,2),p27_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);



% plot third leg

    plot3([p31_training(N,1),p32_training(N,1)],[p31_training(N,2),p32_training(N,2)],[p31_training(N,3),p32_training(N,3)],'color','k');
    plot3([p32_training(N,1),p34_training(N,1)],[p32_training(N,2),p34_training(N,2)],[p32_training(N,3),p34_training(N,3)],'color','k');
    plot3([p32_training(N,1),p33_training(N,1)],[p32_training(N,2),p33_training(N,2)],[p32_training(N,3),p33_training(N,3)],'color','k');
    plot3([p33_training(N,1),p35_training(N,1)],[p33_training(N,2),p35_training(N,2)],[p33_training(N,3),p35_training(N,3)],'color','k');
    plot3([p34_training(N,1),p35_training(N,1)],[p34_training(N,2),p35_training(N,2)],[p34_training(N,3),p35_training(N,3)],'color','k');
    plot3([p35_training(N,1),p36_training(N,1)],[p35_training(N,2),p36_training(N,2)],[p35_training(N,3),p36_training(N,3)],'color','k');
    plot3([p36_training(N,1),p37_training(N,1)],[p36_training(N,2),p37_training(N,2)],[p36_training(N,3),p37_training(N,3)],'color','k');
    
    plot3(p31_training(N,1),p31_training(N,2),p31_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p32_training(N,1),p32_training(N,2),p32_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p33_training(N,1),p33_training(N,2),p33_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p34_training(N,1),p34_training(N,2),p34_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p35_training(N,1),p35_training(N,2),p35_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p36_training(N,1),p36_training(N,2),p36_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p37_training(N,1),p37_training(N,2),p37_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);


% plot fourth leg
    plot3([p41_training(N,1),p42_training(N,1)],[p41_training(N,2),p42_training(N,2)],[p41_training(N,3),p42_training(N,3)],'color','k');
    plot3([p42_training(N,1),p44_training(N,1)],[p42_training(N,2),p44_training(N,2)],[p42_training(N,3),p44_training(N,3)],'color','k');
    plot3([p42_training(N,1),p43_training(N,1)],[p42_training(N,2),p43_training(N,2)],[p42_training(N,3),p43_training(N,3)],'color','k');
    plot3([p43_training(N,1),p45_training(N,1)],[p43_training(N,2),p45_training(N,2)],[p43_training(N,3),p45_training(N,3)],'color','k');
    plot3([p44_training(N,1),p45_training(N,1)],[p44_training(N,2),p45_training(N,2)],[p44_training(N,3),p45_training(N,3)],'color','k');
    plot3([p45_training(N,1),p46_training(N,1)],[p45_training(N,2),p46_training(N,2)],[p45_training(N,3),p46_training(N,3)],'color','k');
    plot3([p46_training(N,1),p47_training(N,1)],[p46_training(N,2),p47_training(N,2)],[p46_training(N,3),p47_training(N,3)],'color','k');
    
    plot3(p41_training(N,1),p41_training(N,2),p41_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p42_training(N,1),p42_training(N,2),p42_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p43_training(N,1),p43_training(N,2),p43_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p44_training(N,1),p44_training(N,2),p44_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p45_training(N,1),p45_training(N,2),p45_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p46_training(N,1),p46_training(N,2),p46_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p47_training(N,1),p47_training(N,2),p47_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);

    hold off
    
view([17,13,7]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
legend('Joint 1 in legs','Joint 2 in legs','Joint 3 in legs','Joint 4 in legs',...
    'Joint 5 in legs','Joint 6 in legs','Joint 7 in legs');
% title(sprintf('Training Data with Markersize %.1f',s));
title(sprintf('%d sets of Training Data - robot sitting down',N));



%%
% % plot the point on the back
% scatter3(pb1(1,:),pb1(2,:),pb1(3,:));

% separate data to training data and validation data


% pb1_training = pb1_training(:,1:0.8*N);
% x_validation = x_training(:,(0.8*N+1):N);
% pb1_validation = x_training(:,0.8*N+1:N);
% x_training = x_training(:,1:0.8*N);


%%
% train neural network
% net = feedforwardnet(100);
% net = train(net,pb1_training,x_training);

% save('trainedNN.mat','net');
load('trainedNN.mat');
%%

% set trajectories on the spline
n = 20;
pb = linspace(0.595,0.33,n);

for i = 1:n
pb1_traj(i,:) = [0,0,pb(i)];
end

%%

% analytical way to solve for pb

%{
% create initial guess
w = [130 60 0]*pi/180;
% x0 = [w w w w 0.55*180/pi 0.15*180/pi]*pi/180;
x0 = [w w w w];

% obtain q in each leg
for i = 1:n

x(i,:) = func_getQ(x0,pb1_traj(i,:));
% set new initial guess for next time step
x0 = x(i,:);

end

% use forward kinematics to calculate all the positions of each joint in
% the robot


for i = 1:n
    
    [~,~,~,~,~,~,...
     p11(i,:),p12(i,:),p13(i,:),p14(i,:),p15(i,:),p16(i,:),p17(i,:),...
     p21(i,:),p22(i,:),p23(i,:),p24(i,:),p25(i,:),p26(i,:),p27(i,:)...
     p31(i,:),p32(i,:),p33(i,:),p34(i,:),p35(i,:),p36(i,:),p37(i,:)...
     p41(i,:),p42(i,:),p43(i,:),p44(i,:),p45(i,:),p46(i,:),p47(i,:)...
     pb1_analytical(i,:),pb2_analytical(i,:),pb3_analytical(i,:)] = func_fwd_kinematics(x(i,:));
    
end

%}
%%

% transpose for the NN
pb1_trajNN = pb1_traj';

Q = net(pb1_trajNN);
% transpose for the fwd kinematics
x = Q';

% use fwd to generate position of each joint during robot sitting down
for i = 1:n
    
    [~,~,~,~,~,~,...
     p11(i,:),p12(i,:),p13(i,:),p14(i,:),p15(i,:),p16(i,:),p17(i,:),...
     p21(i,:),p22(i,:),p23(i,:),p24(i,:),p25(i,:),p26(i,:),p27(i,:)...
     p31(i,:),p32(i,:),p33(i,:),p34(i,:),p35(i,:),p36(i,:),p37(i,:)...
     p41(i,:),p42(i,:),p43(i,:),p44(i,:),p45(i,:),p46(i,:),p47(i,:)...
     pb1(i,:),pb2(i,:),pb3(i,:)] = func_fwd_kinematics(x(i,:));
    
end


%% generate foot trajectories
% calculate the hip and foot position after sitting down
p17expand = p17(end,:);
p11expand = p11(end,:);

p27expand = p27(end,:);
p21expand = p21(end,:);

p37expand = p37(end,:);
p31expand = p31(end,:);

p47expand = p47(end,:);
p41expand = p41(end,:);

% calculate the radius of the leg expansion
temp1 = p11expand-p17expand;
% calculate the radius of the leg expansion
temp2 = p17expand+[0 temp1(2) 0];%hip move the foot plane
% vector from foot to temp hip
temp3 = temp2 - p11expand;
r1 = norm(temp3);
theta1 = acos(p17expand(3)/r1);

% calculate the radius of the leg expansion
temp4 = p21expand-p27expand;
% calculate the radius of the leg expansion
temp5 = p27expand+[0 temp4(2) 0];%hip move the foot plane
% vector from foot to temp hip
temp6 = temp5 - p21expand;
r2 = norm(temp6);
theta2 = acos(p27expand(3)/r2);

% calculate the radius of the leg expansion
temp7 = p31expand-p37expand;
% calculate the radius of the leg expansion
temp8 = p37expand+[0 temp7(2) 0];%hip move the foot plane
% vector from foot to temp hip
temp9 = temp8 - p31expand;
r3 = norm(temp9);
theta3 = acos(p37expand(3)/r3);

% calculate the radius of the leg expansion
temp10 = p41expand-p47expand;
% calculate the radius of the leg expansion
temp11 = p47expand+[0 temp10(2) 0];%hip move the foot plane
% vector from foot to temp hip
temp12 = temp11 - p41expand;
r4 = norm(temp12);
theta4 = acos(p47expand(3)/r4);


% generate the traj for leg expansion
traj_theta1 = linspace(theta1,pi/2,n);
traj_theta2 = linspace(theta2,pi/2,n);
traj_theta3 = linspace(theta3,pi/2,n);
traj_theta4 = linspace(theta4,pi/2,n);
for i = 1:n
traj_leg1(i,:) = temp2 + [r1*sin(traj_theta1(i)) 0 -r1*cos(traj_theta1(i))];
traj_leg2(i,:) = temp5 + [-r2*sin(traj_theta2(i)) 0 -r2*cos(traj_theta2(i))];
traj_leg3(i,:) = temp8 + [r3*sin(traj_theta3(i)) 0 -r3*cos(traj_theta3(i))];
traj_leg4(i,:) = temp11 + [-r4*sin(traj_theta4(i)) 0 -r4*cos(traj_theta4(i))];
end

traj_feet = [traj_leg1'; traj_leg2'; traj_leg3'; traj_leg4'];


%%
% generate training data for leg expansion

q11_training = 0.97+0.01*rand(N,1);
q12_training = 0.68+0.03*rand(N,1);
q13_training = -0.085-1.51*rand(N,1);

q21_training = 0.97+0.01*rand(N,1);
q22_training = 0.68+0.03*rand(N,1);
q23_training = 0.085+1.51*rand(N,1);

q31_training = 1.07+0.01*rand(N,1);
q32_training = 1.09+0.01*rand(N,1);
q33_training = -0.085-1.51*rand(N,1);

q41_training = 1.07+0.01*rand(N,1);
q42_training = 1.09+0.01*rand(N,1);
q43_training = 0.085+1.51*rand(N,1);

x_training = [q11_training q12_training q13_training...
              q21_training q22_training q23_training...
              q31_training q32_training q33_training...
              q41_training q42_training q43_training];

 for i = 1:N
   [p11_training(i,:),p12_training(i,:),p13_training(i,:),p14_training(i,:),p15_training(i,:),p16_training(i,:),p17_training(i,:),...
    p21_training(i,:),p22_training(i,:),p23_training(i,:),p24_training(i,:),p25_training(i,:),p26_training(i,:),p27_training(i,:),...
    p31_training(i,:),p32_training(i,:),p33_training(i,:),p34_training(i,:),p35_training(i,:),p36_training(i,:),p37_training(i,:),...
    p41_training(i,:),p42_training(i,:),p43_training(i,:),p44_training(i,:),p45_training(i,:),p46_training(i,:),p47_training(i,:)]= func_fwd_expand(x_training(i,:),p17expand,p27expand,p37expand,p47expand);
 end
 
 
 %%
 
%  plot the training data leg expanding
figure;
joint1 = scatter3(p11_training(:,1),p11_training(:,2),p11_training(:,3),s,'MarkerFaceColor','k');
hold on
joint2 = scatter3(p12_training(:,1),p12_training(:,2),p12_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
joint3 = scatter3(p13_training(:,1),p13_training(:,2),p13_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
joint4 = scatter3(p14_training(:,1),p14_training(:,2),p14_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
joint5 = scatter3(p15_training(:,1),p15_training(:,2),p15_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
joint6 = scatter3(p16_training(:,1),p16_training(:,2),p16_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
joint7 = scatter3(p17_training(:,1),p17_training(:,2),p17_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p21_training(:,1),p21_training(:,2),p21_training(:,3),s,'MarkerFaceColor','k');
scatter3(p22_training(:,1),p22_training(:,2),p22_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p23_training(:,1),p23_training(:,2),p23_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p24_training(:,1),p24_training(:,2),p24_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p25_training(:,1),p25_training(:,2),p25_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p26_training(:,1),p26_training(:,2),p26_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p27_training(:,1),p27_training(:,2),p27_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p31_training(:,1),p31_training(:,2),p31_training(:,3),s,'MarkerFaceColor','k');
scatter3(p32_training(:,1),p32_training(:,2),p32_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p33_training(:,1),p33_training(:,2),p33_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p34_training(:,1),p34_training(:,2),p34_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p35_training(:,1),p35_training(:,2),p35_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p36_training(:,1),p36_training(:,2),p36_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p37_training(:,1),p37_training(:,2),p37_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');

scatter3(p41_training(:,1),p41_training(:,2),p41_training(:,3),s,'MarkerFaceColor','k');
scatter3(p42_training(:,1),p42_training(:,2),p42_training(:,3),s,'MarkerFaceColor','[0.1,0.5,0.9]');
scatter3(p43_training(:,1),p43_training(:,2),p43_training(:,3),s,'MarkerFaceColor','[0.9,0.5,0.1]');
scatter3(p44_training(:,1),p44_training(:,2),p44_training(:,3),s,'MarkerFaceColor','[0.5,0.2,0.1]');
scatter3(p45_training(:,1),p45_training(:,2),p45_training(:,3),s,'MarkerFaceColor','[0.7,0.7,0.7]');
scatter3(p46_training(:,1),p46_training(:,2),p46_training(:,3),s,'MarkerFaceColor','[0.5,0.5,0.5]');
scatter3(p47_training(:,1),p47_training(:,2),p47_training(:,3),s,'MarkerFaceColor','[0.3,0.9,0.5]');
% plot first leg
    plot3([p11_training(N,1),p12_training(N,1)],[p11_training(N,2),p12_training(N,2)],[p11_training(N,3),p12_training(N,3)],'color','k');
    plot3([p12_training(N,1),p14_training(N,1)],[p12_training(N,2),p14_training(N,2)],[p12_training(N,3),p14_training(N,3)],'color','k');
    plot3([p12_training(N,1),p13_training(N,1)],[p12_training(N,2),p13_training(N,2)],[p12_training(N,3),p13_training(N,3)],'color','k');
    plot3([p13_training(N,1),p15_training(N,1)],[p13_training(N,2),p15_training(N,2)],[p13_training(N,3),p15_training(N,3)],'color','k');
    plot3([p14_training(N,1),p15_training(N,1)],[p14_training(N,2),p15_training(N,2)],[p14_training(N,3),p15_training(N,3)],'color','k');
    plot3([p15_training(N,1),p16_training(N,1)],[p15_training(N,2),p16_training(N,2)],[p15_training(N,3),p16_training(N,3)],'color','k');
    plot3([p16_training(N,1),p17_training(N,1)],[p16_training(N,2),p17_training(N,2)],[p16_training(N,3),p17_training(N,3)],'color','k');
    
    plot3(p11_training(N,1),p11_training(N,2),p11_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p12_training(N,1),p12_training(N,2),p12_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p13_training(N,1),p13_training(N,2),p13_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p14_training(N,1),p14_training(N,2),p14_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p15_training(N,1),p15_training(N,2),p15_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p16_training(N,1),p16_training(N,2),p16_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p17_training(N,1),p17_training(N,2),p17_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);

% plot second leg
    plot3([p21_training(N,1),p22_training(N,1)],[p21_training(N,2),p22_training(N,2)],[p21_training(N,3),p22_training(N,3)],'color','k');
    plot3([p22_training(N,1),p24_training(N,1)],[p22_training(N,2),p24_training(N,2)],[p22_training(N,3),p24_training(N,3)],'color','k');
    plot3([p22_training(N,1),p23_training(N,1)],[p22_training(N,2),p23_training(N,2)],[p22_training(N,3),p23_training(N,3)],'color','k');
    plot3([p23_training(N,1),p25_training(N,1)],[p23_training(N,2),p25_training(N,2)],[p23_training(N,3),p25_training(N,3)],'color','k');
    plot3([p24_training(N,1),p25_training(N,1)],[p24_training(N,2),p25_training(N,2)],[p24_training(N,3),p25_training(N,3)],'color','k');
    plot3([p25_training(N,1),p26_training(N,1)],[p25_training(N,2),p26_training(N,2)],[p25_training(N,3),p26_training(N,3)],'color','k');
    plot3([p26_training(N,1),p27_training(N,1)],[p26_training(N,2),p27_training(N,2)],[p26_training(N,3),p27_training(N,3)],'color','k');
    
    plot3(p21_training(N,1),p21_training(N,2),p21_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p22_training(N,1),p22_training(N,2),p22_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p23_training(N,1),p23_training(N,2),p23_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p24_training(N,1),p24_training(N,2),p24_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p25_training(N,1),p25_training(N,2),p25_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p26_training(N,1),p26_training(N,2),p26_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p27_training(N,1),p27_training(N,2),p27_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);



% plot third leg

    plot3([p31_training(N,1),p32_training(N,1)],[p31_training(N,2),p32_training(N,2)],[p31_training(N,3),p32_training(N,3)],'color','k');
    plot3([p32_training(N,1),p34_training(N,1)],[p32_training(N,2),p34_training(N,2)],[p32_training(N,3),p34_training(N,3)],'color','k');
    plot3([p32_training(N,1),p33_training(N,1)],[p32_training(N,2),p33_training(N,2)],[p32_training(N,3),p33_training(N,3)],'color','k');
    plot3([p33_training(N,1),p35_training(N,1)],[p33_training(N,2),p35_training(N,2)],[p33_training(N,3),p35_training(N,3)],'color','k');
    plot3([p34_training(N,1),p35_training(N,1)],[p34_training(N,2),p35_training(N,2)],[p34_training(N,3),p35_training(N,3)],'color','k');
    plot3([p35_training(N,1),p36_training(N,1)],[p35_training(N,2),p36_training(N,2)],[p35_training(N,3),p36_training(N,3)],'color','k');
    plot3([p36_training(N,1),p37_training(N,1)],[p36_training(N,2),p37_training(N,2)],[p36_training(N,3),p37_training(N,3)],'color','k');
    
    plot3(p31_training(N,1),p31_training(N,2),p31_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p32_training(N,1),p32_training(N,2),p32_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p33_training(N,1),p33_training(N,2),p33_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p34_training(N,1),p34_training(N,2),p34_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p35_training(N,1),p35_training(N,2),p35_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p36_training(N,1),p36_training(N,2),p36_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p37_training(N,1),p37_training(N,2),p37_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);


% plot fourth leg
    plot3([p41_training(N,1),p42_training(N,1)],[p41_training(N,2),p42_training(N,2)],[p41_training(N,3),p42_training(N,3)],'color','k');
    plot3([p42_training(N,1),p44_training(N,1)],[p42_training(N,2),p44_training(N,2)],[p42_training(N,3),p44_training(N,3)],'color','k');
    plot3([p42_training(N,1),p43_training(N,1)],[p42_training(N,2),p43_training(N,2)],[p42_training(N,3),p43_training(N,3)],'color','k');
    plot3([p43_training(N,1),p45_training(N,1)],[p43_training(N,2),p45_training(N,2)],[p43_training(N,3),p45_training(N,3)],'color','k');
    plot3([p44_training(N,1),p45_training(N,1)],[p44_training(N,2),p45_training(N,2)],[p44_training(N,3),p45_training(N,3)],'color','k');
    plot3([p45_training(N,1),p46_training(N,1)],[p45_training(N,2),p46_training(N,2)],[p45_training(N,3),p46_training(N,3)],'color','k');
    plot3([p46_training(N,1),p47_training(N,1)],[p46_training(N,2),p47_training(N,2)],[p46_training(N,3),p47_training(N,3)],'color','k');
    
    plot3(p41_training(N,1),p41_training(N,2),p41_training(N,3),'o','MarkerFaceColor','k','MarkerSize',5);
    plot3(p42_training(N,1),p42_training(N,2),p42_training(N,3),'o','MarkerFaceColor','[0.1,0.5,0.9]','MarkerSize',5);
    plot3(p43_training(N,1),p43_training(N,2),p43_training(N,3),'o','MarkerFaceColor','[0.9,0.5,0.1]','MarkerSize',5);
    plot3(p44_training(N,1),p44_training(N,2),p44_training(N,3),'o','MarkerFaceColor','[0.5,0.2,0.1]','MarkerSize',5);
    plot3(p45_training(N,1),p45_training(N,2),p45_training(N,3),'o','MarkerFaceColor','[0.7,0.7,0.7]','MarkerSize',5);
    plot3(p46_training(N,1),p46_training(N,2),p46_training(N,3),'o','MarkerFaceColor','[0.5,0.5,0.5]','MarkerSize',5);
    plot3(p47_training(N,1),p47_training(N,2),p47_training(N,3),'o','MarkerFaceColor','[0.3,0.9,0.5]','MarkerSize',5);

    hold off
    
view([17,13,7]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
legend('Joint 1 in legs','Joint 2 in legs','Joint 3 in legs','Joint 4 in legs',...
    'Joint 5 in legs','Joint 6 in legs','Joint 7 in legs');
% title(sprintf('Training Data with Markersize %.1f',s));
title(sprintf('%d sets of Training Data - robot expanding the legs',N));
 
 
 %%
 
 
 

%    transpose the matrix for NN
x_training = x_training';

p11_training = p11_training';
p12_training = p12_training';
p13_training = p13_training';
p14_training = p14_training';
p15_training = p15_training';
p16_training = p16_training';
p17_training = p17_training';

p21_training = p21_training';
p22_training = p22_training';
p23_training = p23_training';
p24_training = p24_training';
p25_training = p25_training';
p26_training = p26_training';
p27_training = p27_training';

p31_training = p31_training';
p32_training = p32_training';
p33_training = p33_training';
p34_training = p34_training';
p35_training = p35_training';
p36_training = p36_training';
p37_training = p37_training';

p41_training = p41_training';
p42_training = p42_training';
p43_training = p43_training';
p44_training = p44_training';
p45_training = p45_training';
p46_training = p46_training';
p47_training = p47_training';


p_feet = [p11_training;p21_training;p31_training;p41_training];

%     net2 = feedforwardnet(100);
%     net2 = train(net,p_feet,x_training);

% save('trainedNN_ExpandLeg.mat','net2');

load('trainedNN_ExpandLeg.mat');
    y = net(traj_feet);
    
    y = y';
    
    
%     use fwd to calculate the position of the robot when expanding the leg
    for i = 1:n
%expand the leg
    [p11(n+i,:),p12(n+i,:),p13(n+i,:),p14(n+i,:),p15(n+i,:),p16(n+i,:),p17(n+i,:),...
     p21(n+i,:),p22(n+i,:),p23(n+i,:),p24(n+i,:),p25(n+i,:),p26(n+i,:),p27(n+i,:)...
     p31(n+i,:),p32(n+i,:),p33(n+i,:),p34(n+i,:),p35(n+i,:),p36(n+i,:),p37(n+i,:)...
     p41(n+i,:),p42(n+i,:),p43(n+i,:),p44(n+i,:),p45(n+i,:),p46(n+i,:),p47(n+i,:)...
     ] = func_fwd_expand(y(i,:),p17expand,p27expand,p37expand,p47expand);

    end
    

    
    x = [x ;y];
 
%% 



%{
% add n more sets of joints for expanding the legs
y = zeros(2*n,12);

% for the first n sets of data, stay the same, and for the second n sets
% of datas, only turning the hip joints
y(:,1) = [x(:,1);x(end,1)*ones(n,1)];
y(:,2) = [x(:,2);x(end,2)*ones(n,1)];
y(:,3) = [x(:,3);(linspace(x(end,3),-90/180*3.14,n)).'];
y(:,4) = [x(:,4);x(end,4)*ones(n,1)];
y(:,5) = [x(:,5);x(end,5)*ones(n,1)];
y(:,6) = [x(:,6);(linspace(x(end,6),90/180*3.14,n)).'];
y(:,7) = [x(:,7);x(end,7)*ones(n,1)];
y(:,8) = [x(:,8);x(end,8)*ones(n,1)];
y(:,9) = [x(:,9);(linspace(x(end,9),-90/180*3.14,n)).'];
y(:,10) = [x(:,10);x(end,10)*ones(n,1)];
y(:,11) = [x(:,11);x(end,11)*ones(n,1)];
y(:,12) = [x(:,12);(linspace(x(end,12),90/180*3.14,n)).'];


x = y;

% calculate the position of the leg expansion, and put them in the position vector
for i = 1:n
%expand the leg
    [p11(n+i,:),p12(n+i,:),p13(n+i,:),p14(n+i,:),p15(n+i,:),p16(n+i,:),p17(n+i,:),...
     p21(n+i,:),p22(n+i,:),p23(n+i,:),p24(n+i,:),p25(n+i,:),p26(n+i,:),p27(n+i,:)...
     p31(n+i,:),p32(n+i,:),p33(n+i,:),p34(n+i,:),p35(n+i,:),p36(n+i,:),p37(n+i,:)...
     p41(n+i,:),p42(n+i,:),p43(n+i,:),p44(n+i,:),p45(n+i,:),p46(n+i,:),p47(n+i,:)...
     ] = func_fwd_expand(x(n+i,:),p17expand,p27expand,p37expand,p47expand);

end


%}
%%
% set the auxiliary line
h = 0.02;
hspline = p17(n,3);

p171 = p17 + [0 0 -h];
p271 = p27 + [0 0 -h];
p371 = p37 + [0 0 -h];
p471 = p47 + [0 0 -h];
p127 = (p17+p27)./2;
p347 = (p37+p47)./2;

% top of the belly
pmdl = (p127+p347)./2';
% bottom of the belly
pbtm = [pmdl(:,1:2) pmdl(:,3)-hspline];

pb1 = [pb1;pb1(end,:).*ones(n,3)];
pb2 = [pb2;pb2(end,:).*ones(n,3)];
pb3 = [pb3;pb3(end,:).*ones(n,3)];

%%
% plot the animation
pb1_traj = pb1_traj';
filename = 'sim_mechanismNN.gif';
for i = 1:n*2
    
% plot 3d of the robot
    ax10 = subplot(2,2,1:2);
% %     plot the trajectory
    ax_traj = plot3([pb1_traj(1,1),pb1_traj(1,end)],[pb1_traj(2,1),pb1_traj(2,end)],[pb1_traj(3,1),pb1_traj(3,end)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
%     plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     
    
% %     plot points on the spine
    ax_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);

% plot first leg
    
    
    ax11 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
%     hold on
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax12 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax13 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax14 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    
    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0.8706    0.3725    0.1569]);
    
    
    
    

    hold off
view(ax10,[17,10,7]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax10,'3D View');
legend('boxoff');
legend([ax_traj, ax_follow,ax11,ax12,ax13,ax14,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');
box on





%%
% plot the side view
ax30 = subplot(2,2,3);
% %     plot the trajectory
    ax3_traj = plot3([pb1_traj(1,1),pb1_traj(1,end)],[pb1_traj(2,1),pb1_traj(2,end)],[pb1_traj(3,1),pb1_traj(3,end)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
%     plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     
    
% %     plot points on the spine
    ax3_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%    
    
    ax31 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
    hold on
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax32 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax33 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax34 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    

    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
    belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0 0.4470 0.7410]);
    
    
    hold off
view(ax30,[10,0,0]);
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.4 0.4]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax30,'Side View');
legend('boxoff');
legend([ax3_traj, ax3_follow,ax31,ax32,ax33,ax34,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');
box on

%%
 %-----------------------------------------------------------------------------
% plot front view
ax40 = subplot(2,2,4);

% %     plot the trajectory
    ax4_traj = plot3([pb1_traj(1,1),pb1_traj(1,end)],[pb1_traj(2,1),pb1_traj(2,end)],[pb1_traj(3,1),pb1_traj(3,end)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
    hold on
%     plot3([pb2_traj(1,1),pb2_traj(end,1)],[pb2_traj(1,2),pb2_traj(end,2)],[pb2_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     plot3([pb3_traj(1,1),pb3_traj(end,1)],[pb3_traj(1,2),pb3_traj(end,2)],[pb3_traj(1,3),pb3_traj(end,3)],':r','linewidth',0.8,'Color',[0.6350 0.0780 0.1840]);
%     
    
% %     plot points on the spine
    ax4_follow = plot3(pb1(i,1),pb1(i,2),pb1(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb2(i,1),pb2(i,2),pb2(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%     plot3(pb3(i,1),pb3(i,2),pb3(i,3),'o','MarkerSize',3,'color',[0.9290 0.6940 0.1250]);
%    
    ax41 = plot3([p11(i,1),p12(i,1)],[p11(i,2),p12(i,2)],[p11(i,3),p12(i,3)],'color','k');
    hold on 
    plot3([p12(i,1),p14(i,1)],[p12(i,2),p14(i,2)],[p12(i,3),p14(i,3)],'color','k');
    plot3([p12(i,1),p13(i,1)],[p12(i,2),p13(i,2)],[p12(i,3),p13(i,3)],'color','k');
    plot3([p13(i,1),p15(i,1)],[p13(i,2),p15(i,2)],[p13(i,3),p15(i,3)],'color','k');
    plot3([p14(i,1),p15(i,1)],[p14(i,2),p15(i,2)],[p14(i,3),p15(i,3)],'color','k');
    plot3([p15(i,1),p16(i,1)],[p15(i,2),p16(i,2)],[p15(i,3),p16(i,3)],'color','k');
    plot3([p16(i,1),p17(i,1)],[p16(i,2),p17(i,2)],[p16(i,3),p17(i,3)],'color','k');
    
    plot3(p11(i,1),p11(i,2),p11(i,3),'o','color','k','MarkerSize',5);
    plot3(p12(i,1),p12(i,2),p12(i,3),'o','color','k','MarkerSize',5);
    plot3(p13(i,1),p13(i,2),p13(i,3),'o','color','k','MarkerSize',5);
    plot3(p14(i,1),p14(i,2),p14(i,3),'o','color','k','MarkerSize',5);
    plot3(p15(i,1),p15(i,2),p15(i,3),'o','color','k','MarkerSize',5);
    plot3(p16(i,1),p16(i,2),p16(i,3),'o','color','k','MarkerSize',5);
    plot3(p17(i,1),p17(i,2),p17(i,3),'o','color','k','MarkerSize',5);
    

% plot second leg
    ax42 = plot3([p21(i,1),p22(i,1)],[p21(i,2),p22(i,2)],[p21(i,3),p22(i,3)],'color','m');
 
    plot3([p22(i,1),p24(i,1)],[p22(i,2),p24(i,2)],[p22(i,3),p24(i,3)],'color','m');
    plot3([p22(i,1),p23(i,1)],[p22(i,2),p23(i,2)],[p22(i,3),p23(i,3)],'color','m');
    plot3([p23(i,1),p25(i,1)],[p23(i,2),p25(i,2)],[p23(i,3),p25(i,3)],'color','m');
    plot3([p24(i,1),p25(i,1)],[p24(i,2),p25(i,2)],[p24(i,3),p25(i,3)],'color','m');
    plot3([p25(i,1),p26(i,1)],[p25(i,2),p26(i,2)],[p25(i,3),p26(i,3)],'color','m');
    plot3([p26(i,1),p27(i,1)],[p26(i,2),p27(i,2)],[p26(i,3),p27(i,3)],'color','m');
    
    plot3(p21(i,1),p21(i,2),p21(i,3),'o','color','m','MarkerSize',5);
    plot3(p22(i,1),p22(i,2),p22(i,3),'o','color','m','MarkerSize',5);
    plot3(p23(i,1),p23(i,2),p23(i,3),'o','color','m','MarkerSize',5);
    plot3(p24(i,1),p24(i,2),p24(i,3),'o','color','m','MarkerSize',5);
    plot3(p25(i,1),p25(i,2),p25(i,3),'o','color','m','MarkerSize',5);
    plot3(p26(i,1),p26(i,2),p26(i,3),'o','color','m','MarkerSize',5);
    plot3(p27(i,1),p27(i,2),p27(i,3),'o','color','m','MarkerSize',5);
    
     


% plot third leg
    ax43 = plot3([p31(i,1),p32(i,1)],[p31(i,2),p32(i,2)],[p31(i,3),p32(i,3)],'color','g');
 
    plot3([p32(i,1),p34(i,1)],[p32(i,2),p34(i,2)],[p32(i,3),p34(i,3)],'color','g');
    plot3([p32(i,1),p33(i,1)],[p32(i,2),p33(i,2)],[p32(i,3),p33(i,3)],'color','g');
    plot3([p33(i,1),p35(i,1)],[p33(i,2),p35(i,2)],[p33(i,3),p35(i,3)],'color','g');
    plot3([p34(i,1),p35(i,1)],[p34(i,2),p35(i,2)],[p34(i,3),p35(i,3)],'color','g');
    plot3([p35(i,1),p36(i,1)],[p35(i,2),p36(i,2)],[p35(i,3),p36(i,3)],'color','g');
    plot3([p36(i,1),p37(i,1)],[p36(i,2),p37(i,2)],[p36(i,3),p37(i,3)],'color','g');
    
    plot3(p31(i,1),p31(i,2),p31(i,3),'o','color','g','MarkerSize',5);
    plot3(p32(i,1),p32(i,2),p32(i,3),'o','color','g','MarkerSize',5);
    plot3(p33(i,1),p33(i,2),p33(i,3),'o','color','g','MarkerSize',5);
    plot3(p34(i,1),p34(i,2),p34(i,3),'o','color','g','MarkerSize',5);
    plot3(p35(i,1),p35(i,2),p35(i,3),'o','color','g','MarkerSize',5);
    plot3(p36(i,1),p36(i,2),p36(i,3),'o','color','g','MarkerSize',5);
    plot3(p37(i,1),p37(i,2),p37(i,3),'o','color','g','MarkerSize',5);
    
 

% plot fourth leg
    ax44 = plot3([p41(i,1),p42(i,1)],[p41(i,2),p42(i,2)],[p41(i,3),p42(i,3)],'color','c');

    plot3([p42(i,1),p44(i,1)],[p42(i,2),p44(i,2)],[p42(i,3),p44(i,3)],'color','c');
    plot3([p42(i,1),p43(i,1)],[p42(i,2),p43(i,2)],[p42(i,3),p43(i,3)],'color','c');
    plot3([p43(i,1),p45(i,1)],[p43(i,2),p45(i,2)],[p43(i,3),p45(i,3)],'color','c');
    plot3([p44(i,1),p45(i,1)],[p44(i,2),p45(i,2)],[p44(i,3),p45(i,3)],'color','c');
    plot3([p45(i,1),p46(i,1)],[p45(i,2),p46(i,2)],[p45(i,3),p46(i,3)],'color','c');
    plot3([p46(i,1),p47(i,1)],[p46(i,2),p47(i,2)],[p46(i,3),p47(i,3)],'color','c');
    
    plot3(p41(i,1),p41(i,2),p41(i,3),'o','color','c','MarkerSize',5);
    plot3(p42(i,1),p42(i,2),p42(i,3),'o','color','c','MarkerSize',5);
    plot3(p43(i,1),p43(i,2),p43(i,3),'o','color','c','MarkerSize',5);
    plot3(p44(i,1),p44(i,2),p44(i,3),'o','color','c','MarkerSize',5);
    plot3(p45(i,1),p45(i,2),p45(i,3),'o','color','c','MarkerSize',5);
    plot3(p46(i,1),p46(i,2),p46(i,3),'o','color','c','MarkerSize',5);
    plot3(p47(i,1),p47(i,2),p47(i,3),'o','color','c','MarkerSize',5);
    
    
% % plot spline2
ps = plot3([p17(i,1),p27(i,1)],[p17(i,2),p27(i,2)],[p17(i,3),p27(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p47(i,1),p37(i,1)],[p47(i,2),p37(i,2)],[p47(i,3),p37(i,3)],'color',[0 0.4470 0.7410]);
    plot3([p127(i,1),p347(i,1)],[p127(i,2),p347(i,2)],[p127(i,3),p347(i,3)],'color',[0 0.4470 0.7410]);
    belly = plot3([pmdl(i,1),pbtm(i,1)],[pmdl(i,2),pbtm(i,2)],[pmdl(i,3),pbtm(i,3)],'color',[0 0.4470 0.7410]);
    
    hold off
view(ax40,[0,10,0])
xlabel('x');
ylabel('y');
zlabel('z');
xlim([-0.45 0.45]);
ylim([-0.45 0.5]);
zlim([-0.1 0.8]);
title(ax40,'Front View');
legend('boxoff');
legend([ax4_traj, ax4_follow,ax41,ax42,ax43,ax44,ps,belly],'trajectory',['following-trajectory' newline 'points on the robot'],'1st leg','2nd leg','3rd leg','4th leg','spine','belly');

box on


% set the window size
set(gcf,'position',[100,60,1300,870]);

% pause(0.2);
       frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    

end

%%
% plot the joint trajectories
figure;
y = x(:,1:12).*180/pi;
ax_q = plot(y);
% plot(x(:,1));
lgd = legend(ax_q,'q11','q12','q13','q21','q22','q23','q31','q32','q33','q41','q42','q43');
lgd.NumColumns = 4;
title('Joint Trajectories for 4 Legs');
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
xlim([-1 2*n+2]);
ylim([-100 145]);

figure;
% plot joint trajectories with separated subplot

subplot(2,2,1);
ax_trjact1 = plot(x(:,(1:3)).*180/pi);
legend('q11','q12','q13');
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 1');
legend('boxoff');

subplot(2,2,2);
ax_trjact2 = plot(x(:,(4:6)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 2');
legend('q21','q22','q23');
legend('boxoff');

subplot(2,2,3);
ax_trjact3 = plot(x(:,(7:9)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 3');
legend('q31','q32','q33');
legend('boxoff');

subplot(2,2,4);
ax_trjact4 = plot(x(:,(10:12)).*180/pi);
xlim([-1 2*n+2]);
ylim([-100 145]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Joints Trajectories in Leg 4');
legend('q41','q42','q43');
legend('boxoff');

%% plot hip trajectories
figure;
hip_traj1 = plot(x(:,3).*180/pi);
hold on
hip_traj2 = plot(x(:,6).*180/pi);
hip_traj3 = plot(x(:,9).*180/pi);
hip_traj4 = plot(x(:,12).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title('Hip Trajectories');
legend([hip_traj1,hip_traj2,hip_traj3,hip_traj4],'q13','q23','q33','q43');
legend('boxoff');

%%plot hip trajectories\
figure;
subplot(1,2,1);
hip_traj1 = plot(x(:,3).*180/pi);
hold on
hip_traj2 = plot(x(:,6).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title(['Hip Trajectories in' newline 'First and Second Leg']);
legend([hip_traj1,hip_traj2],'q13','q23');
legend('boxoff');

subplot(1,2,2);
hip_traj3 = plot(x(:,9).*180/pi);
hold on
hip_traj4 = plot(x(:,12).*180/pi);
hold off
xlim([-1 2*n+2]);
ylim([-100 100]);
xlabel('[time steps(n)]');
ylabel('[angle(degrees)]');
title(['Hip Trajectories in' newline 'Third and Fourth Leg']);
legend([hip_traj3,hip_traj4],'q33','q43');
legend('boxoff');


%%

% plot the error on the pb1 based on the NN and traj

    figure;
    pb1_traj = pb1_traj';
    pb1_error = pb1(1:n,:)-pb1_traj;
    plot(pb1_error(:,3));
    title('trajectory error - sitting down phase');
    xlabel('time step [n]');
    ylabel('error [m]');  
    xlim([-1 n+2]);
    
%     plot(pb1(1:n,3));
%     hold on 
%     pb1_traj = pb1_traj';
%     plot(pb1_traj(:,3));
%     hold off


traj_expand_error1 = p11(n+1:end,:) - traj_leg1;
traj_expand_error2 = p21(n+1:end,:) - traj_leg2;
traj_expand_error3 = p31(n+1:end,:) - traj_leg3;
traj_expand_error4 = p41(n+1:end,:) - traj_leg4;

% calculate the trajectory error
traj_expand_error_length1 = sqrt(traj_expand_error1(:,1).^2+traj_expand_error1(:,2).^2+traj_expand_error1(:,3).^2);
traj_expand_error_length2 = sqrt(traj_expand_error2(:,1).^2+traj_expand_error2(:,2).^2+traj_expand_error2(:,3).^2);
traj_expand_error_length3 = sqrt(traj_expand_error3(:,1).^2+traj_expand_error3(:,2).^2+traj_expand_error3(:,3).^2);
traj_expand_error_length4 = sqrt(traj_expand_error4(:,1).^2+traj_expand_error4(:,2).^2+traj_expand_error4(:,3).^2);

figure
% suptitle('trajectory error when expanding the leg');

    subplot(2,2,1);
    plot(traj_expand_error_length1);
    title('trajectory error - foot of leg 1');
    xlabel('time step [n]');
    ylabel('error [m]');  
    xlim([-1 n+2]);
%     ylim([0.3 2]*10^(-3));
    
    subplot(2,2,2);
    plot(traj_expand_error_length2);
    title('trajectory error - foot of leg 2');
    xlabel('time step [n]');
    ylabel('error [m]');  
    xlim([-1 n+2]);
    
    subplot(2,2,3);
    plot(traj_expand_error_length3);
    title('trajectory error - foot of leg 3');
    xlabel('time step [n]');
    ylabel('error [m]');  
    xlim([-1 n+2]);
    
    subplot(2,2,4);
    plot(traj_expand_error_length4);
    title('trajectory error - foot of leg 4');
    xlabel('time step [n]');
    ylabel('error [m]');  
    xlim([-1 n+2]);
    





